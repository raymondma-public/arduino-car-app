package com.ust.regan.arduinocarapp;

import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.utils.FillFormatter;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

import io.palaima.smoothbluetooth.Device;
import io.palaima.smoothbluetooth.SmoothBluetooth;


public class MainActivity extends AppCompatActivity {


    //====================================SeekBar var
    SeekBar seekBar;
    //====================================Chart var
    final int DATA_COUNT = 100;
    private Queue<Float> data = new ArrayDeque<>(DATA_COUNT);

    int currentSpeed = 0;

    private static final float REAL_MAX_SPEED=4.081f;
    private static final float REAL_MIN_SPEED=0f;

    private static final int MAX_SPEED = 255;
    private static final int MIN_SPEED = 80;

    private static final int MAX_TIME = 100;
    private static final int MIN_TIME = 0;


    //======================================BT
    Button connectButton;
    public static final int ENABLE_BT__REQUEST = 1;
    private SmoothBluetooth mSmoothBluetooth;
    private TextView mStateTv;
    private TextView mDeviceTv;

    private List<Integer> mBuffer = new ArrayList<>();
    private List<String> mResponseBuffer = new ArrayList<>();

    private TextView speedTV;
    private String speedStringFormat="%.2f";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        speedTV=(TextView)findViewById(R.id.speedTV);
        String speedString=String.format(speedStringFormat, 0f);
        speedTV.setText(Html.fromHtml("Speed: "+speedString+" cm s<sup>-1</sup>"));

        //==============================Chart onCreate
        final LineChart lineChart = (LineChart) findViewById(R.id.chart_line);
        lineChart.setDescription("");
        lineChart.setFillFormatter(new FillFormatter() {
            @Override
            public float getFillLinePosition(LineDataSet dataSet, LineData data, float chartMaxY, float chartMinY) {
                return 0;
            }
        });
        YAxis yAxis = lineChart.getAxisLeft();
        yAxis.setStartAtZero(false);
        yAxis.setAxisMaxValue(REAL_MAX_SPEED);
        yAxis.setAxisMinValue(REAL_MIN_SPEED);

        YAxis yAxis2 = lineChart.getAxisRight();
        yAxis2.setStartAtZero(false);
        yAxis2.setAxisMaxValue(REAL_MAX_SPEED);
        yAxis2.setAxisMinValue(REAL_MIN_SPEED);

        seekBar = (SeekBar) findViewById(R.id.mySeekBar);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                if(progress>0){
                    currentSpeed =(MIN_SPEED)+((MAX_SPEED-MIN_SPEED))*progress/100;
                }else{
                    currentSpeed = 0;
                }

                if(speedTV!=null){
                    float realSpeedData=currentSpeed*REAL_MAX_SPEED/MAX_SPEED;
                    String speedString=String.format(speedStringFormat, realSpeedData);
                    speedTV.setText(Html.fromHtml("Speed: "+speedString+" cm s<sup>-1</sup>"));
                }

                Log.d("Slide Bar", "currentSpeed=" + currentSpeed+" progress="+progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        lineChart.setFillFormatter(null);

        for (int i = 0; i < DATA_COUNT; i++) {
            data.add(0f);
        }
        lineChart.setData(getLineData());
        new Thread() {
            @Override
            public void run() {
                while (true) {
                    try {

                        lineChart.setData(getLineData());
                        lineChart.postInvalidate();
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.start();



        //======================================BT onCreate
        //======================================BT send signal to Car
        new Thread() {
            @Override
            public void run() {
                while (true) {
                    try {

                        if (mSmoothBluetooth != null && mSmoothBluetooth.isBluetoothAvailable()) {


                                String formatted = String.format("*%03d", currentSpeed);
                                Log.d("Values","currentSpeed="+currentSpeed+" , formatted="+formatted);//+" char="+formatted.charAt(0));
                                mSmoothBluetooth.send(formatted);

                        }
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.start();




        mSmoothBluetooth = new SmoothBluetooth(this, SmoothBluetooth.ConnectionTo.OTHER_DEVICE, SmoothBluetooth.Connection.INSECURE, null);


        mSmoothBluetooth.setListener(mListener);

        connectButton = (Button) findViewById(R.id.connectBtn);
        connectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSmoothBluetooth != null && !mSmoothBluetooth.isConnected()) {
                    mSmoothBluetooth.tryConnection();
                } else if (mSmoothBluetooth != null && mSmoothBluetooth.isConnected()) {
                    mSmoothBluetooth.disconnect();
                    mResponseBuffer.clear();

                }

            }
        });

        mStateTv = (TextView) findViewById(R.id.state);
        mStateTv.setText("Disconnected");
        mDeviceTv = (TextView) findViewById(R.id.device);


    }

    public void onStart() {
        super.onStart();
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
    }

    public void onDestroy() {
        super.onDestroy();
        //============BT onDestroy
        mSmoothBluetooth.stop();
    }



    @Override
    protected void onResume() {
        super.onResume();
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
    }



    //=================================Chart methods
    private List<Entry> getChartData() {
        final int DATA_COUNT = 5;

        float currentData = currentSpeed;
        if (!data.isEmpty()) {
            data.remove();
        }

        float realSpeedData=currentData*REAL_MAX_SPEED/MAX_SPEED;

        data.add(realSpeedData);

        List<Entry> chartData = new ArrayList<>();

        for (int i = data.size(); i <= DATA_COUNT; i++) {
            chartData.add(new Entry(Math.round(0), i));
        }

        int i = 0;
        for (Float f : data) {
                chartData.add(new Entry(f, i));
            i++;
        }

        return chartData;
    }

    private List<String> getLabels() {

        List<String> chartLabels = new ArrayList<>();
        for (int i = DATA_COUNT; i >= 0; i--) {
            if (i % 10 == 0) {

                chartLabels.add("" + -(i/10)+" s");
            } else {
                chartLabels.add("");
            }
        }
        return chartLabels;
    }

    private LineData getLineData() {
        LineDataSet dataSetA = new LineDataSet(getChartData(), "Speed");

        List<LineDataSet> dataSets = new ArrayList<>();
        dataSets.add(dataSetA); // add the datasets

        return new LineData(getLabels(), dataSets);
    }


    //===================================BT methods

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ENABLE_BT__REQUEST) {
            if (resultCode == RESULT_OK) {
                mSmoothBluetooth.tryConnection();
            }
        }
    }

    private SmoothBluetooth.Listener mListener = new SmoothBluetooth.Listener() {
        @Override
        public void onBluetoothNotSupported() {
            Toast.makeText(MainActivity.this, "Bluetooth not found", Toast.LENGTH_SHORT).show();
            finish();
        }

        @Override
        public void onBluetoothNotEnabled() {
            Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBluetooth, ENABLE_BT__REQUEST);
        }

        @Override
        public void onConnecting(Device device) {
            mStateTv.setText("Connecting to");
            mDeviceTv.setText(device.getName());
        }

        @Override
        public void onConnected(Device device) {
            mStateTv.setText("Connected to ");
            mDeviceTv.setText(device.getName());
            MainActivity.this.connectButton.setText("Disonnect");
        }

        @Override
        public void onDisconnected() {
            mStateTv.setText("Disconnected");
            mDeviceTv.setText("");
            try {
                MainActivity.this.connectButton.setText("Connect");
            } catch (Exception e) {
                Log.e("onDisconnect", e.getMessage(), e);
            }
        }

        @Override
        public void onConnectionFailed(Device device) {
            mStateTv.setText("Disconnected");
            mDeviceTv.setText("");
            if (device != null) {
                Toast.makeText(MainActivity.this, "Failed to connect to " + device.getName(), Toast.LENGTH_SHORT).show();

                if (device.isPaired()) {
                    mSmoothBluetooth.doDiscovery();
                }
            }
            MainActivity.this.connectButton.setText("Connect");
        }

        @Override
        public void onDiscoveryStarted() {
            Toast.makeText(MainActivity.this, "Searching", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onDiscoveryFinished() {

        }

        @Override
        public void onNoDevicesFound() {
            Toast.makeText(MainActivity.this, "No device found", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onDevicesFound(final List<Device> deviceList,
                                   final SmoothBluetooth.ConnectionCallback connectionCallback) {

            Device car = null;
            for (Device d : deviceList) {
                if (d.getName().equals("BT04-A") && d.getAddress().equals("AB:03:67:78:27:8E")) {
                    Toast.makeText(getApplicationContext(), "[" + d.getAddress() + ", " + d.getName() + "] is connected", Toast.LENGTH_LONG);
                    car = d;
                    break;
                }
            }

            if (car != null) {
                connectionCallback.connectTo(car);
            }

        }

        @Override
        public void onDataReceived(int data) {
            mBuffer.add(data);
            if (data == 62 && !mBuffer.isEmpty()) {
                //if (data == 0x0D && !mBuffer.isEmpty() && mBuffer.get(mBuffer.size()-2) == 0xA0) {
                StringBuilder sb = new StringBuilder();
                for (int integer : mBuffer) {
                    sb.append((char) integer);
                }
                mBuffer.clear();
                mResponseBuffer.add(0, sb.toString());
//                mResponsesAdapter.notifyDataSetChanged();
            }
        }
    };
}
