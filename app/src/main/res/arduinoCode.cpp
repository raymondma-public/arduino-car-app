#include<math.h>
#include<Servo.h>

#include <SoftwareSerial.h>
#define MAX_BTCMDLEN 128
SoftwareSerial Conn(10, 11); //RX, TX


byte cmd[MAX_BTCMDLEN]; // received 128 bytes from an Android system

int len = 0; // received command length
char val;
char buffer [4];

const int DIR1_RIGHT = 12;
const int DIR2_RIGHT = 11;
const int DIR1_LEFT = 8;
const int DIR2_LEFT = 9;

const int PWM_LEFT = 6;
const int PWM_RIGHT = 5;

Servo myservo;
int ServoPin = 10;
int pos = 0;
int MotorAdjustmengPin = A1;
#define MOTORADJUSTMENT
void setup() {

  pinMode(DIR1_RIGHT, OUTPUT);
  pinMode(DIR2_RIGHT, OUTPUT);
  pinMode(DIR1_LEFT, OUTPUT);
  pinMode(DIR2_LEFT, OUTPUT);

  pinMode(PWM_LEFT, OUTPUT);
  pinMode(PWM_RIGHT, OUTPUT);
  Serial.begin(9600);
  Conn.begin(9600);
}
void loop() {
//  Serial.println("loop");
  myservo.attach(ServoPin);
  myservo.write(90);
  int speed = 0;

 if (Serial.available()) {

    val = Serial.readBytes(buffer,3);
    speed=charToInt();
    Serial.print(speed);
  }


  motorsWrite(speed, speed);

  //  while (Conn.available()) {
  //      Serial.println(Conn.readString());
  //      Conn.println("Test Transmission String.");
  //    }
//  char str[MAX_BTCMDLEN];
//
//  int insize, ii;
//  int tick = 0;
//  while ( tick < MAX_BTCMDLEN ) { // 因為包率同為9600, Android送過來的字元可能被切成數份
//    if ( (insize = (Conn.available())) > 0 ) { // 讀取藍牙訊息
//      for ( ii = 0; ii < insize; ii++ ) {
//        cmd[(len++) % MAX_BTCMDLEN] = char(Conn.read());
//      }
//    } else {
//      tick++;
//    }
//  }
//
//  if ( len ) { // 用串列埠顯示從Android手機傳過來的訊息
//    sprintf(str, "%s", cmd);
//    Serial.println(str);
//    cmd[0] = '\0';
//  }
//
//  len = 0;




  //delay(250);
}
void motorsWrite(int speedLeft, int speedRight)
{
  float motorAdjustment = MotorAdjustment();
  if (motorAdjustment < 0) {
    speedRight *= (1 + motorAdjustment);
  }
  else {
    speedLeft *= (1 - motorAdjustment);
  }
  if (speedRight > 0)
  {
    digitalWrite(DIR1_RIGHT, 0);
    digitalWrite(DIR2_RIGHT, 1);
  }
  else
  {
    digitalWrite(DIR1_RIGHT, 1);
    digitalWrite(DIR2_RIGHT, 0);
  }
  analogWrite(PWM_RIGHT, abs(speedRight));

  if (speedLeft > 0)
  {
    digitalWrite(DIR1_LEFT, 0);
    digitalWrite(DIR2_LEFT, 1);
  }
  else
  {
    digitalWrite(DIR1_LEFT, 1);
    digitalWrite(DIR2_LEFT, 0);
  }
  analogWrite(PWM_LEFT, abs(speedLeft));
}

void stopMotor()
{
  motorsWrite(0, 0);
}
float  MotorAdjustment() {
#ifdef MOTORADJUSTMENT
  float motorAdjustment = map(analogRead(MotorAdjustmengPin), 0, 1023, -30, 30) / 100.0;
  return motorAdjustment;
#else
  return 0;
#endif
}


int charToInt()
{
int tmp =0;
for(int i=0;i<3;i++)
{
tmp = tmp * 10 + (buffer[i] - 48);
}
return tmp;
}


